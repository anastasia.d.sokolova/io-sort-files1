package solution;

import java.util.*;

public class MergeSort {

    public MergeSort() {}


    private <T extends Comparable> LinkedList<T> merge(final Deque<T> left, final Deque<T> right) {
        final LinkedList<T> merged = new LinkedList<>();
        while (!left.isEmpty() && !right.isEmpty()) {
            if (left.peek().compareTo(right.peek()) <= 0) {
                merged.add(left.pop());
            } else {
                merged.add(right.pop());
            }
        }
        merged.addAll(left);
        merged.addAll(right);
        return merged;
    }


    public void sort(final LinkedList<String> input, String type) {
        if(input.size()!=0) {
            if (type.equals("Integer")) {
                LinkedList<Integer> integerInput = new LinkedList<>();
                for (int i = 0; i < input.size(); i++) {
                    integerInput.add(Integer.valueOf(input.get(i)));
                }
                input.clear();
                divine(integerInput);
                for (int i = 0; i < integerInput.size(); i++) {
                    input.add(integerInput.get(i).toString());
                }
            } else {
                divine(input);
            }
        }
    }


    public <T extends Comparable> void divine(final LinkedList<T> input) {
        if (input.size() != 1) {
            final LinkedList<T> left = new LinkedList<>();
            final LinkedList<T> right = new LinkedList<>();
            boolean logicalSwitch = true;
            while (!input.isEmpty()) {
                if (logicalSwitch) {
                    left.add(input.pop());
                } else {
                    right.add(input.pop());
                }
                logicalSwitch = !logicalSwitch;
            }
            divine(left);
            divine(right);
            input.addAll(merge(left, right));
        }
    }

}
